# api591

#591 Rent data API  
#591 Rent data crawler   
#implement with django  
#implement with mongodb  


## Description

An django base API for querying 591 rent_data

## version check

django == 3.2  
mongodb == 5.0.6  
python == 3.9.7  

## Package

reqeusts,  
re,  
selenium(webdriver with chrome),  
pymongo,


## Getting Start

* start DB service
```
brew services start mongodb-community
```

* start django service
```
python manage.py runserver
```


## Data updating strategy

start crawler manually first time to build up the DB
```
python crawler_setup.py
```

using crontab to update db content ervery 30 minutes

```
# edit crontab content
crontab -e
```

```
# runnig every 30 mins
*/30 * * * * python crawler_update.py 
```

## DB structure

Db --> data591  
       Collection --> rent_data  
                      columns --> (ID,位置,出租者,出租者身份,聯絡電話,型態,現況,性別要求)  


## API Requirement

【設計/建立 RESTful API】供查詢下列資訊: 【以 JSON 格式回傳，請自訂 Schema】

- [x] 【男生可承租】且【位於新北】的租屋物件 ==> set req_gender, loc filter
- [x]  以【聯絡電話】查詢租屋物件 ==> set tel filter
- [x]  所有【非屋主自行刊登】的租屋物件 ==> not_owner filter
- [x]  【臺北】【屋主為女性】【姓氏為吳】所刊登的所有租屋物件 ==> set owner_gender, fname filter


## API detail

GET: http://127.0.0.1:8000/data/

API Parameters: 

req_gender=> require gender
* "male" means 限男生 in 591.com 
* "female" means 限女生 in 591.com

loc=> location
* "TPC" means 台北市 in 591.com 
* "NTC" means 新北市 in 591.com

tel=> telephone number
* "XXXX-XXX-XXX" (eg: 0932-287-991)

not_owner=> 
* True means the contact person is not the house owner
* False means the contact person is the house owner

owner_gender=>
* Under the condition the contact person is the house owner, looking for the owners with specific gender,
* (eg: "female")

fname=>
* looking for the contact person with specific family name,
* in chinese (eg: "吳")

example:
* GET: http://127.0.0.1:8000/data/?req_gender=male&loc=NTC&tel=0908-999-869&not_owner=0&owner_gender=female&fname=趙

## Project status

Completed 

