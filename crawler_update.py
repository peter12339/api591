#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 16:17:49 2022

@author: linjingkai
"""


import requests
import json
import re
from selenium import webdriver
from pymongo import MongoClient

city_dict={
	"台北市":"1",
	"新北市":"3"
}

#取得csrd-token
driver = webdriver.Chrome("/Users/linjingkai/Desktop/chromedriver")
driver.get('https://rent.591.com.tw/?kind=0&region=1')
data = driver.page_source
csrf = re.findall('''<meta name="csrf-token" content=".*"''', data)
csrf = re.findall('''content=".*"''', csrf[0])
csrf = csrf[0].split('"')[1]


#取得cookie
cookies = driver.get_cookies()
cookie_string=""
for i in cookies:
    cookie_string = cookie_string + i['name']+"="+i["value"]+";"


#設定header抓取頁面所需
headers_page={
"Accept":"application/json, text/javascript, */*; q=0.01",
"Connection":"keep-alive",
"X-CSRF-TOKEN":"DECZhI51bHzx707kWuzPqpSqFmmAWcMGpRTYjPaC",
"X-Requested-With":"XMLHttpRequest",
"Referer":"https://rent.591.com.tw/?kind=0&region=1",
"Host":"rent.591.com.tw",
"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36",
"Accept-Encoding":"gzip, deflate, br",
"Accept-Language":"en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7",
"sec-ch-ua":'''" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"''',
"sec-ch-ua-mobile":"?0",
"sec-ch-ua-platform":"macOS",
"Sec-Fetch-Dest":"empty",
"Sec-Fetch-Mode":"cors",
"Sec-Fetch-Site":"same-origin"
}
headers_page["X-CSRF-TOKEN"]=csrf
headers_page["Cookie"]=cookie_string

#設定headers抓取物件所需
headers_obj={
"Accept":"application/json, text/javascript, */*; q=0.01",
"Connection":"keep-alive",
"X-CSRF-TOKEN":"DECZhI51bHzx707kWuzPqpSqFmmAWcMGpRTYjPaC",
"Referer":"https://rent.591.com.tw/?kind=0&region=1",
"Host":"bff.591.com.tw",
"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36",
"Accept-Encoding":"gzip, deflate, br",
"Accept-Language":"en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7",
"sec-ch-ua":'''" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"''',
"sec-ch-ua-mobile":"?0",
"sec-ch-ua-platform":"macOS",
"Sec-Fetch-Dest":"empty",
"Sec-Fetch-Mode":"cors",
"Sec-Fetch-Site":"same-origin",
"token":"s4ni8aq3nb6ore7edfmdhisvh3",
"device":"pc",
"deviceid":"s4ni8aq3nb6ore7edfmdhisvh3"
}
headers_obj["X-CSRF-TOKEN"]=csrf

driver.quit()


def get_total_number(city,headers):
    url = "https://rent.591.com.tw/home/search/rsList?is_format_data=1&is_new_list=1&type=1&kind=0&region="+city_dict[city]         
    data = requests.get(url, headers = headers)
    data = data.text
    data = json.loads(data)
    total_number = data['records']
    total_number = int(re.sub("[^0-9]", "", total_number))
    return total_number


def get_page_data(city,headers,page,total_number):
    url = "https://rent.591.com.tw/home/search/rsList?is_format_data=1&is_new_list=1&type=1&kind=0&region="+city_dict[city]+"&firstRow="+str((page-1)*30)+"&totalRows="+str(total_number)       
    data = requests.get(url, headers = headers)
    data = data.text
    data = json.loads(data)
    data = data["data"]["data"]
    return data

def get_obj_detail(obj_id,headers):
    url="https://bff.591.com.tw/v1/house/rent/detail?id="+obj_id
    data = requests.get(url,headers=headers)
    data = data.text
    data = json.loads(data)
    return data["data"]

def formating_data(city,data_page,data_obj):
    full_data = {}
    full_data["ID"]=(data_page["post_id"])
    full_data["位置"]=city
    full_data["出租者"]=(data_page["contact"])
    full_data["出租者身份"]=(data_page["role_name"])
    full_data["聯絡電話"]=(data_obj["linkInfo"]["mobile"])
    full_data["型態"]=(data_obj["info"][3]["value"])
    full_data["現況"]=(data_obj["info"][0]["value"])
    if "限男生" in data_obj['service']['rule']:
        full_data["性別要求"]=('限男生')
        return full_data
    if "限女生" in data_obj['service']['rule']:
        full_data["性別要求"]=('限女生')
        return full_data
    else:
        full_data["性別要求"]=('男女皆可')
        return full_data
        
    
def update_page_data(city,headers,page,total_number):
    url = "https://rent.591.com.tw/home/search/rsList?is_format_data=1&is_new_list=1&type=1&kind=0&region="+city_dict[city]+"&firstRow="+str((page-1)*30)+"&totalRows="+str(total_number)+"orderType=desc"      
    data = requests.get(url, headers = headers)
    data = data.text
    data = json.loads(data)
    data = data["data"]["data"]
    return data
    
    
    
    
def insert_into_DB(data):
    db=MongoClient()
    db = db.get_database("data591")
    db = db.get_collection("rent_data")
    db.insert_many(data)
    print("DB insert success.")
    
    
    
    

def main(city,update,headers_page,headers_obj):
    if update:
        total_number = get_total_number(city,headers_page)
        total_pages = int(total_number/30)+1
        result=[]
        for i in range(total_pages):
            data_page = update_page_data(city,headers_page,i+1,total_number)
            finish=0
            for j in data_page:
                refresh_time = re.sub('\D', '', j['refresh_time'])
                # 30分鍾內更新
                if int(refresh_time)<=30:  
                    data_obj = get_obj_detail(str(j['post_id']),headers_obj)
                    result.append(formating_data(city,j,data_obj))
                else:
                    finish=1
                    break
            if finish:
                break
            print("page "+str(i+1)+" of "+str(total_pages)+" done")
        insert_into_DB(result)
        return 
    else:
        total_number = get_total_number(city,headers_page)
        total_pages = int(total_number/30)+1
        result=[]
        for i in range(total_pages):
            data_page = get_page_data(city,headers_page,i+1,total_number)
            for j in data_page:
                data_obj = get_obj_detail(str(j['post_id']),headers_obj)
                result.append(formating_data(city,j,data_obj))
            print("page "+str(i+1)+" of "+str(total_pages)+" done")
        insert_into_DB(result)
        return 





main(city="台北市",update=True,headers_page=headers_page,headers_obj=headers_obj)
main(city="新北市",update=True,headers_page=headers_page,headers_obj=headers_obj)








