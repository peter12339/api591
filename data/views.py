from base64 import encode
from json import encoder
from django.shortcuts import render
from django.http import JsonResponse
from pymongo import MongoClient #使用pymongo連接mongodb
from bson.json_util import dumps,loads
import re




db=MongoClient()  # 連接Mongodb資料庫
db = db.get_database("data591") # 取得目標資料庫


def dataformating(request):
    
    if request.method == "GET": # 使用http GET method 
        
        request_dict = {} # 資料庫查找條件字典

        #性別要求filter
        gender=request.GET.get("req_gender")
        if gender:
            if gender=="male":
                request_dict["性別要求"]="限男生"
            if gender=="female":
                request_dict["性別要求"]="限女生"

        #地點要求filter
        location=request.GET.get("loc")
        if location:
            if location=="TPC":
                request_dict["位置"]="台北市"
            else:
                request_dict["位置"]="新北市"

        #電話要求filter
        tel = request.GET.get("tel")
        if tel:
            request_dict["聯絡電話"]=tel
        


        #聯絡人為屋主與否filter (條件為除了屋主以外，故使用$ne語法)
        not_owner=request.GET.get('not_owner')
        
        if not_owner:
            request_dict["出租者身份"] = {"$ne":"屋主"}
        else:
            #屋主性別要求filter
            #屋主姓氏要求filter
            owner_gender = request.GET.get('owner_gender')
            fname = request.GET.get('fname')
            
            if fname:
                if owner_gender=="female":
                    request_dict["出租者身份"] = "屋主"
                    request_dict["出租者"] = fname+"小姐"
                if owner_gender=="male":
                    request_dict["出租者身份"] = "屋主"
                    request_dict["出租者"] = fname+"先生"
                else:
                    reg = re.compile(fname+"..", re.IGNORECASE) 
                    request_dict["出租者"] = reg #使用Regular Expression進行判斷

            else:
                if owner_gender=="female":
                    reg = re.compile(".小姐", re.IGNORECASE)
                    request_dict["出租者"] = reg #使用Regular Expression進行判斷
                if owner_gender=="male":
                    request_dict["出租者身份"] = "屋主"
                    reg = re.compile(".先生", re.IGNORECASE)
                    request_dict["出租者"] = reg #使用Regular Expression進行判斷

        
        
        
            

        data  = db.get_collection('rent_data').find(request_dict) # 針對條件進行資料查找
        data = list(data)
        data = dumps(data, ensure_ascii=False) # 將Mongodb crusor object 轉換成 serialized json
        return JsonResponse(data, safe = False) # 以json方式回傳
    else:
        return JsonResponse({"msg":"GET method only"})
    

